#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    oscReceiver.setup(port);

    video.load("/home/kf/Videos/22tone/ludusmicrotonalis2.mp4");
    video.setLoopState(OF_LOOP_NONE);
}

//--------------------------------------------------------------
void ofApp::update(){
    while(oscReceiver.hasWaitingMessages()) {
        ofxOscMessage m;
        oscReceiver.getNextMessage(m);

        if (m.getAddress() == "/setPaused") {
            bool playing = m.getArgAsBool(0);
            video.setPaused(playing);
        }
        
        if (m.getAddress() == "/volume") {
            volume = m.getArgAsFloat(0);
            video.setVolume(volume);
        }

        if (m.getAddress() == "/alpha") {
            alpha =  m.getArgAsFloat(0);
        }
    }

    if (video.isPlaying()) video.update();
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofSetColor(255);
    video.draw(0, 0, ofGetWidth(), ofGetHeight());

    ofEnableAlphaBlending();
    int a = (1 - alpha) * 255;
    ofSetColor(0, 0, 0, a);
    ofDrawRectangle(0, 0, ofGetWidth(), ofGetHeight());
    ofDisableAlphaBlending();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    if (key == 'q') 
    {
        video.stop();
        ofExit();
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
